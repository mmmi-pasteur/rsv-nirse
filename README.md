# Effect of nirsevimab on hospitalisations for respiratory syncytial virus bronchiolitis in France, 2023–24: a modelling study

Data and code of the article [Brault et al., Effect of nirsevimab on hospitalisations for respiratory syncytial virus bronchiolitis in France, 2023–24: a modelling study, The Lancet Child & Adolescent Health, 2024](https://www.thelancet.com/journals/lanchi/article/PIIS2352-4642(24)00143-3/abstract).

# Abstract
### Background
Respiratory syncytial virus (RSV) is a major cause of hospitalisations and deaths among infants worldwide. France was one of the first countries to implement a national programme (beginning on Sept 15, 2023) for administration of nirsevimab, a single-dose long-acting monoclonal antibody treatment, to infants born on or after Feb 6, 2023, to prevent lower respiratory tract infection caused by RSV. We aimed to estimate the effectiveness of nirsevimab and the number of hospitalisations averted in children younger than 24 months in real-world settings.
### Methods
In this modelling study, we developed an age-structured deterministic model characterising RSV transmission as well as plausible scenarios for the administration of nirsevimab doses based on maternity ward and community pharmacy supply data. We retrospectively estimated nirsevimab effectiveness in infants younger than 24 months during the 2023–24 RSV season in France (excluding overseas territories) and the number of averted hospitalisations for RSV bronchiolitis occurring after emergency department visits, by calibrating the model to hospital and virological surveillance data from Aug 21, 2017, to Feb 4, 2024, alongside serological data from a previous cross-sectional study. To assess the robustness of our estimates, we conducted sensitivity analyses in which we modified our assumptions about the number of doses administered, the reconstruction of the number of RSV-associated hospitalisations for bronchiolitis, the duration of maternal and post-infection immunity to RSV, and the number of contacts in children aged 0–2 months.
### Findings
We estimated that nirsevimab administration prevented 5800 (95% credible interval 3700–7800) RSV-associated hospitalisations for bronchiolitis after emergency department visits among children younger than 24 months, including 4200 (2900–5600) hospitalisations among those aged 0–2 months, between Sept 15, 2023 (the date nirsevimab was introduced), and Feb 4, 2024—a 23% (16–30) reduction in the total number of hospitalisations and a 35% (25–44) reduction in the 0–2 months age group, compared with the scenario without administration. In our baseline scenario, in which we estimated that 215 000 doses of nirsevimab were administered by Jan 31, 2024, the estimated effectiveness against RSV-associated hospitalisations for bronchiolitis was 73% (61–84), corresponding to one hospitalisation averted for every 39 (26–54) doses administered. In sensitivity analyses, nirsevimab remained effective against RSV-associated hospitalisations for bronchiolitis after emergency department attendance.
### Interpretation
Our findings show that nirsevimab administration campaigns could effectively reduce the RSV-related hospital burden of bronchiolitis in children younger than 24 months.

# Data 
The [data/](data/) folder contains the data needed to produce the results of the article:

### Demographic data
- [dailyBirthsMetroFrance.csv](data/births/dailyBirthsMetroFrance.csv): the daily number of births in France, excluding overseas territories, was obtained by interpolating the monthly number of births.
- [mortalityRateFranceMetro4q.csv](data/mortalityRate/mortalityRateFranceMetro4q.csv): the mortality/migration rate by age group.
- [popMetroFrance1975_2022_4q.csv](data/populations/popMetroFrance1975_2022_4q.csv): the population in France (excluding overseas territories) from 1975 to 2022 for the age groups 0-2 months, 3-5 months, 6-8 months, 9-11 months, 12-23 months, 24-35 months, and $\geq$ 36 months.

### Contact matrices
- [contactMatrixFrance4q.csv](data/contactMatrices/contactMatrixFrance4q.csv): the contact matrix used in the baseline scenario with age groups 0-2 months, 3-5 months, 6-8 months, 9-11 months, 12-23 months, 24-35 months, and $\geq$ 36 months.
- [contactMatrixFrance4qRed.csv](data/contactMatrices/contactMatrixFrance4qRed.csv): the contact matrix assuming 50% reduction of contact in 0-2 months age group with respect to baseline scenario.
- [contactMatrixFrance85ageGroups.csv](data/contactMatrices/contactMatrixFrance85ageGroups.csv): the contact matrix in France estimated in [Mistry et al., Nature Communications, 2021](https://www.nature.com/articles/s41467-020-20544-y). The baseline contact matrix [contactMatrixFrance4q.csv](data/contactMatrices/contactMatrixFrance4q.csv) was computed from this one.

### Surveillance data
- [metroBronchioByAgeCodeCorr.csv](data/hospitalisation/metroBronchioByAgeCodeCorr.csv): the daily number of hospitalisations by age for bronchiolitis after emergency room visits in France (excluding overseas territories) estimated based on the number of facilities and emergency visits coded in the OSCOUR system.
- [metroBronchioByAgeCodeCorrRenalNormPeakHosp.csv](data/hospitalisation/metroBronchioByAgeCodeCorrRenalNormPeakHosp.csv) : the weekly number of RSV-associated bronchiolitis hospitalisations after emergency room visits in France (excluding overseas territories) under different hypotheses according to the proportion of RSV-associated bronchiolitis at the peak of bronchiolitis hospitalisations.
- [renalFrance.csv](data/renal/renalFrance.csv): the weekly number of positive RSV cases in RENAL network.

### Serological data
- [serology4ag.csv](data/serology/serology4ag.csv): the number of RSV seropositive individuals (column sero) and tested individuals (column tot) in SeroPed study.

### Nirsevimab (Beyfortus) delivery data
- [daily_beyfortus.csv](data/beyfortus/daily_beyfortus.csv): the daily number of nirsevimab (Beyfortus) doses delivered to maternity wards and pharmacies in France (50 mg or 100 mg doses).

# Scripts

### Data processing
- [contactMatrix.R](contactMatrix.R): computes the contact matrices in age group 0-2 months, 3-5 months, 6-8 months, 9-11 months, 12-23 months, 24-35 months, >= 36 months from the contact matrix of age group 0 yo, 1 yo, 2 yo, ..., 84+ yo. 
- [scriptsForMatrixSymmetrization.R](utils/scriptsForMatrixSymmetrization.R): functions to normalise contact matrices.
- [filterBronchioWithRenal.R](filterBronchioWithRenal.R): reconstruction of the weekly RSV-associated bronchiolitis hospitalisations after emergency room visists from OSCOUR and RENAL data.

### Define scenarios
- [defineDynDosesNirse.R](defineDynDosesNirse.R): defines nirsevimab dose administration scenarios.
- [defineScenarios4qNirseBirthAge.R](defineScenarios4qNirseBirthAge.R): scenarios with fixed parameters considered.

### Model
- [sirAgeReinfAgingRenal4qNirseBirthAge.R](odinModels/sirAgeReinfAgingRenal4qNirseBirthAge.R): compartmental RSV 
transmission model.
- [functions.cpp](utils/functions.cpp): includes the seasonal transmission rate function and the age-based probability of hospitalisation upon RSV infection function.

### Run inference procedure
- [runScenarios.R](runScenarios.R): runs inference for all scenarios.
- [runMCMC.R](utils/runMCMC.R): MCMC sampler.

### Plots
- [plotCompareNirse.R](plotCompareNirse.R) : generates plots.

rm(list = ls())
library(tidyverse)

# Compute the daily number of doses administered
admi_nirse <- function(tot_doses, start_treat, break1, break2, end_treat, ratio){
  n_days_break1 <- as.numeric(interval(start_treat, break1), 'days')
  n_days_break2 <- as.numeric(interval(break1, break2), 'days')
  n_days_end <- as.numeric(interval(break2, end_treat), 'days')

  daily_doses1 <-  tot_doses/(ratio * n_days_break2 + n_days_break1 + n_days_end)
  daily_doses2 <- ratio * daily_doses1

  daily_doses <- tibble(date = seq(start_treat, end_treat, 'days'),
        daily_doses = case_when(date < break1 ~ daily_doses1,
                         date >= break1 & date < break2 ~ daily_doses2,
                         date >= break2 & date < end_treat ~ daily_doses1)) %>% 
  complete(date = seq(ymd('2023-09-01'), ymd('2024-02-04'), by = 'day'),
           fill = list(daily_doses = 0)) %>% 
    mutate(doses = tot_doses)
  return(daily_doses)
}

# Scenarios of doses administered
sce_doses <- c(195000, 200000, 205000, 210000, 215000, 220000, 225000)
sce_dyn_doses <- crossing(tibble(start_treat = ymd(c('2023-09-15', '2023-09-15')),
                                 break1 = ymd(c('2023-09-15', '2023-09-15')),
                                 break2 = ymd(c('2024-01-08', '2024-01-16')),
                                 end_treat = ymd(c('2024-02-01', "2024-02-01")),
                                 dyn = c('break_8jan', 'break_16jan')),
                          doses = sce_doses,
                          ratio = c(2))

l_dyn_doses <- list()
for(d in 1:nrow(sce_dyn_doses)){
  sce_select <- sce_dyn_doses %>% 
    slice(d)
  n_doses_tot <- sce_select$doses
  start_treat <- sce_select$start_treat
  break1 <- sce_select$break1
  break2 <- sce_select$break2
  end_treat <- sce_select$end_treat
  ratio <- sce_select$ratio
  
  # dyn <- sce_select$dyn
  
  l_dyn_doses[[d]] <- admi_nirse(n_doses_tot, start_treat, 
                                 break1, break2, end_treat, 
                                 ratio = ratio) %>% 
    mutate(dyn = sce_select$dyn, ratio = ratio)
}

df_dyn_doses <- do.call('rbind', l_dyn_doses)

# Save the scenario of doses administered
df_dyn_doses %>% 
  saveRDS('data/sceDynDoses_v2.rds')



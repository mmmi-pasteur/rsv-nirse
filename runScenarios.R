rm(list = ls())
pacman::p_load(GGally, ggpubr, gridExtra, foreach, doParallel, coda, tidyverse,
               zoo, odin, lubridate, scales, data.table, Rcpp, ISOweek, aweek)
# source('utils/functions.R')
source('utils/scriptsForMatrixSymmetrization.R')
source('utils/runMCMC.R')
sourceCpp('utils/functions.cpp')

################################################################################
# Load the scenario
################################################################################
# Load scenarios
v_sce <- '8'
df_scenarios <- readRDS(str_glue('scenario/model4qNirseBirthAge/scenario_v{v_sce}.rds'))

# Output folder
name_folder <- str_glue('scenarios_v{v_sce}') # name of folder to save results
dir.create(paste0('output/results/model4qNirseBirthAge/', name_folder))
output_calibration <- paste0('output/results/model4qNirseBirthAge/', name_folder, '/')

df_scenarios %>% 
  print(n = 100)

my_arg <- commandArgs(trailingOnly = T)
i <- as.numeric(my_arg[1])
if(is.na(i)) {
  i <- 2
}
df_scenarios

# Select the scenario
sce <- df_scenarios %>% 
  slice(i)
sce

# Parameters of the differents scenarios
start_data <- sce$start_data
end_data <- sce$end_data
# end_treat <- sce$end_treat

n_chains <- sce$nchains 
mcmc_steps <- sce$mcmc_steps # steps by chain
mcmc_adaptive_steps <- sce$mcmc_adapt # adaptive steps
type_loglik <- sce$type_loglik
scale_ar <- sce$scale_ar

fixedParams <- sce$fixedParams[[1]]
# beta <- sce$beta
# mu <- sce$mu

noise <- sce$noise
size <- sce$size
gamma <- sce$gamma
omega <- sce$omega
omega_M <- sce$omega_M
delta1 <- sce$delta1
delta2 <- sce$delta2

eta1 <- sce$eta1
# eta2 <- sce$eta2
alpha_22_23 <- sce$alpha_22_23

seasonality_type <- sce$seasonality_type 
eps_age_ihr_old <- sce$eps_age_ihr_old
light_omega <- sce$light_omega
fitHospLess2yo <- sce$fitHospLess2yo
alpha_school <- sce$alpha_school
peakRSV <- sce$peakRSV
tpeak <- sce$tpeak

n_doses <- sce$doses # number of nirsevimab doses
dynamic <- sce$dyn
slope_ratio <- sce$ratio
target <- sce$target 
ratio_doses_age <- sce$ratio_doses_age
prop_doses_50mg <- sce$prop_doses_50mg  #0.74

cm_type <- sce$cm
################################################################################
# Load and process the data
################################################################################
# Daily births from 1946 to 2022
daily_births <- read_csv('data/births/dailyBirthsMetroFrance.csv') %>% 
  arrange(date)

# Load rate annual mortality rate by age group
df_mr_annual <- read_csv('data/mortalityRate/mortalityRateFranceMetro4q.csv')
df_mr_daily <- df_mr_annual %>%
  mutate(mr = as.numeric(mr)/(365*1000))

# Time serie of population by age in metropolitan France
df_pop <- read_csv('data/populations/popMetroFrance1975_2022_4q.csv') 

################################################################################
# Initialization of the model
################################################################################
start_sim <- ymd('2005-01-01') # First day of the simulation
end_sim <- ymd('2024-06-01')  # Last day of the simulation

if (ymd(sce$end_data) > end_sim){
  stop('ERROR : the end date of simulation has to be larger than the end date of data !')
}

# Initialize the model
# Correspond to expect proportion of pop in each
# S compartement when equilibrium is reached. Use in the model to decorrelate R0 and expo.
pS <- c(0.02, 0.02, 0.96)

# Initial population
pop_ini <- df_pop %>% 
  filter(year == year(start_sim)) %>% 
  pull(pop)

n_age <- length(pop_ini)

# Load contact matrix for metropolitan France
if(cm_type == 'baseline'){
c_mat_raw <- read_csv('data/contactMatrices/contactMatrixFrance4q.csv') %>% 
  pivot_wider(names_from = age_contacts, values_from = mean_contact) %>% 
  dplyr::select(-c('age_individual')) %>% 
  as.matrix()
} else if(cm_type == 'red_2months') {
  c_mat_raw <- read_csv('data/contactMatrices/contactMatrixFrance4qRed.csv') %>% 
    pivot_wider(names_from = age_contacts, values_from = mean_contact) %>% 
    dplyr::select(-c('age_individual')) %>% 
    as.matrix()
} else {
  stop('There is no contact matrix corresponding')
}

# Load rate of mortality
mr <- df_mr_daily %>% 
  pull(mr) 

# Number of days in the simulation
n_days <- as.numeric(interval(start_sim, end_sim), 'days') + 1

# Vector of days to be simulated
interp_ts <- 0:(n_days-1) 
# Dictionary date to number of day
df_dic_dates <- tibble(date = seq(start_sim, end_sim, by = 'days'), days = interp_ts)

################################################################################
# Load and prepare data of hospitalisation for bronchiolitis in metropolitan 
# France 
################################################################################
df_metro <- read_csv('data/hospitalisation/metroBronchioByAgeCodeCorrRenalNormPeakHosp.csv') # data corrected by number of coded events

age_group4q <- c('[0,0.25)', '[0.25,0.5)', '[0.5,0.75)', '[0.75,1)', '[1,2)',
                 '[2,3)', '[3,Inf]')

unique(df_metro$age)

data_hosp_bronchio <- df_metro %>% 
  mutate(iHosp_obs_peak100 = as.integer(n_bronchio_hosp_norm),
       iHosp_obs_peak90 = as.integer(n_bronchio_hosp_norm90),
       iHosp_obs_peak80 = as.integer(n_bronchio_hosp_norm80),
       iHosp_obs_peakMix = as.integer(n_bronchio_hosp_normMix),
       iHosp_obs_peak90_3w = as.integer(n_bronchio_hosp_norm90_3w),
       iHosp_obs_peak90_5w = as.integer(n_bronchio_hosp_norm90_5w)) %>% 
  filter(date >= start_data, date <= end_data) %>% 
  ungroup()

data_hosp_bronchio %>%
  ggplot() +
  facet_wrap(~age, scales = 'free') +
  geom_line(aes(x = date, y = iHosp_obs_peak100))

# Load serology data
df_sero_4ag <- read_csv('data/serology/serology4ag.csv')

df_sero_4ag

################################################################################
# Daily doses of Nirsevimab
################################################################################
# Number of daily doses of beyfortus distributed
df_beyfortus_delivered <- read_csv('data/beyfortus/daily_beyfortus.csv') %>% 
  group_by(date) %>% 
  summarise(doses = sum(doses))

df_admin_nirse <- readRDS('data/sceDynDoses_v2.rds') %>% 
  filter(ratio == slope_ratio, dyn == dynamic, doses == n_doses)

df_daily_injec <- df_admin_nirse %>% 
  complete(date = seq(start_sim, end_sim, by = 'day'),
           fill = list(daily_doses = 0, 
                       dyn = dynamic,
                       doses = n_doses,
                       ratio = slope_ratio))

df_daily_injec %>% 
  print(n = 100)

if(target == 'newborn'){ # All doses administered to newborns
  daily_doses_nb <- df_daily_injec %>% pull(daily_doses)
  daily_doses <- daily_doses_nb * 0
  daily_doses2 <- daily_doses_nb * 0
} else if(target == 'newborn_3months') { # 50mg administered to newborn, 100mg to 0-2 months
  daily_doses_nb <- prop_doses_50mg * df_daily_injec %>% pull(daily_doses)
  daily_doses <- (1-prop_doses_50mg) * df_daily_injec %>% pull(daily_doses)
  daily_doses2 <- daily_doses_nb * 0
} else if(target == 'newborn_6months') { # 50mg administered to newborn, 100mg to 0-2 months
  daily_doses_nb <- prop_doses_50mg * df_daily_injec %>% pull(daily_doses)
  daily_doses <- ratio_doses_age * (1 - prop_doses_50mg) * df_daily_injec %>%  pull(daily_doses)
  daily_doses2 <- (1-ratio_doses_age) * (1 - prop_doses_50mg) * df_daily_injec %>% pull(daily_doses)
} else {
  stop('Error: the target does not exist')
}

################################################################################
# Create a model
################################################################################
if(peakRSV == 'peak100'){
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peak100)
} else if(peakRSV == 'peak90'){
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peak90)
} else if(peakRSV == 'peak80') {
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peak80)
} else if(peakRSV == 'peakMix') {
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peakMix)
} else if(peakRSV == 'peak90_3w'){
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peak90_3w)
}else if(peakRSV == 'peak90_5w'){
  data <- data_hosp_bronchio %>% 
    dplyr::select(date, week, age, iHosp_obs = iHosp_obs_peak90_5w)
} else {
  stop(str_glue('This value of peakRSV {(peakRSV)} does not exist!'))
}

data_sero <- df_sero_4ag

ihr_age_model <- compute_ihr_4q_old_cpp

source('odinModels/sirAgeReinfAgingRenal4qNirseBirthAge.R')

init_model <- list(data = data, data_sero = data_sero, scale_ar = scale_ar,
                   daily_births = daily_births,
                   daily_mortality_rate = mr, c_mat_raw = c_mat_raw, 
                   pop_ini = pop_ini, gamma = gamma, start_sim = start_sim, 
                   end_sim = end_sim,
                   pS = pS, compute_ihr_age = ihr_age_model, 
                   seasonality_type = seasonality_type, 
                   light_omega = light_omega, n_doses = n_doses, 
                   df_daily_injec = df_daily_injec)

model <- create_model_4q_R0_renal_nirseBirthAge(data = data, 
                                        daily_births = daily_births,
                                        daily_mortality_rate = mr, 
                                        c_mat_raw = c_mat_raw,
                                        pop_ini, gamma = gamma, 
                                        start_sim = start_sim,
                                        end_sim = end_sim,
                                        pS = pS, compute_ihr_age = ihr_age_model,
                                        seasonality_type = seasonality_type, 
                                        data_sero = data_sero, 
                                        light_omega = light_omega,
                                        daily_doses = daily_doses,
                                        daily_doses_nb = daily_doses_nb,
                                        daily_doses2 = daily_doses2)

c_names <- model$par_names
inds_pars_to_not_update <- sort(match(fixedParams, c_names))

if(seasonality_type == 'cos'){
  inds_pars_to_not_update <- c(inds_pars_to_not_update, 3)
}

if(fitHospLess2yo == T){
  base_inds_to_update <- c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 
                           17, 20, 21, 22)
} else if (fitHospLess2yo == F) {
  base_inds_to_update <- c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 
                           17, 18, 19, 20, 21, 22)
} else {
  stop('This value of fitHospLess2yo does not exist')
}


if(year(end_data) <= 2020){ # data only before the pandemic
  model$params0 <- c(2, 0.1, 41.5, omega, delta1, 0.13, 
                     1, 2.5, tpeak, eta1, omega_M, size, 1, alpha_school, 
                     eps_age_ihr_old, 1, delta2, 5, 5, 0.9, 1, 1)
  
  if("eps_age_ihr" %in% fixedParams){
    model$params0[which(c_names == "eps_age_ihr")] <- 0
  }
  if("eps_exp_ihr" %in% fixedParams){
    model$params0[which(c_names == "eps_exp_ihr")] <- 0
  }
  if("max_seasonality" %in% fixedParams){
    model$params0[which(c_names == "max_seasonality")] <- 0
  }
  model$inds_to_update <-  Filter(function(x) !(x %in% c(7,13,16,inds_pars_to_not_update)), base_inds_to_update)
  
  print('Prepandemic data')
} else if (year(end_data) > 2020){ # data after the pandemic
  model$params0 <- c(2, 0.4, 41.5, omega, delta1, 0.7, 
                     0.7, 1.3, tpeak, eta1, omega_M, size, 0.98, alpha_school, 
                     eps_age_ihr_old, 0.65, delta2, 5, 5,  0.9, 1, 1)
  
  if("eps_age_ihr" %in% fixedParams){
    model$params0[which(c_names == "eps_age_ihr")] <- 0
  }
  if("eps_exp_ihr" %in% fixedParams){
    model$params0[which(c_names == "eps_exp_ihr")] <- 0
  }
  if("max_seasonality" %in% fixedParams){
    model$params0[which(c_names == "max_seasonality")] <- 0
  }
  model$inds_to_update <-  Filter(function(x) !(x %in% inds_pars_to_not_update), base_inds_to_update)
  
  print('Postpandemic data')
} else {
  stop('ERROR: end_data should be a date')
}
last(data$week)

model$sd_prop <- sd_prop_mcmc <- c(0.00869, 0.0166, 0.00311,
                                   0.00817, 0.0000137, 0.0299, 0.0384, 
                                   0.0342, 0.0000116, 0.1, 0.008, 1.29, 0.00625, 
                                   0.00452, 0.0240, 0.0182, 0.0000376, 0.0378, 0.0352, 
                                   0.533, 0.0250, 0.0149)

################################################################################
# Run MCMC
################################################################################
n_chains <- n_chains # number of chains 
mcmc_steps <- mcmc_steps # steps by chain  
mcmc_adaptive_steps <- mcmc_adaptive_steps # adaptive steps
compute_loglik_mcmc <- function(x) model$compute_loglik(x, type = type_loglik, 
                                                        noise = noise, 
                                                        scale_ar = scale_ar, 
                                                        fitHospLess2yo = fitHospLess2yo)

estimate_time_mcmc <- function(compute_loglik, inds_to_update, steps){
  t_loglik <- system.time(for (i in 1:10) {
    compute_loglik_mcmc(model$params0)
  })
  print(str_glue("Time estimated to process MCMC: {t_loglik['elapsed']/10*length(inds_to_update)*steps/3600} hours"))
}


estimate_time_mcmc(compute_loglik = compute_loglik_mcmc, model$inds_to_update, mcmc_steps)

if(T) {
  n_cores <- detectCores()
  print(str_glue('Cores detected: {n_cores} cores'))
  registerDoParallel(min(n_chains, n_cores))
  source('utils/runMCMC.R')
  set.seed(20220121)
  start_mcmc <- Sys.time()
  lchain <- foreach(icount(n_chains)) %dopar% {
    run_MCMC(compute_loglik_mcmc, model$is_invalid, model$params0,
             inds_to_update = model$inds_to_update,
             mcmc_steps = mcmc_steps, mcmc_adaptive_steps = mcmc_adaptive_steps,
             verbose = TRUE, sd_proposal = model$sd_prop)
  }
  elapsed <- as.double(Sys.time() - start_mcmc, units = "hours")
  print(paste("Running time ->", elapsed, "hours"))
  
  
  for (k in 1:n_chains){
    colnames(lchain[[k]]$params) <- c_names
    colnames(lchain[[k]]$accept) <- c_names
  }
  
  df_chains <- do.call('rbind', lchain) %>% 
    as_tibble() %>%
    mutate(n_chain = 1:n_chains)
  
  df_loglik <- df_chains %>% 
    dplyr::select(loglik, n_chain) %>% 
    unnest(loglik) %>% 
    unnest(loglik) %>% 
    group_by(n_chain) %>% 
    mutate(iter = 1:n()) %>% 
    ungroup()
  
  df_params <- df_chains %>% 
    dplyr::select(params) %>% 
    unnest(c(params)) %>% 
    pull(params) %>% 
    as_tibble()
  
  colnames(df_params) <- c_names
  df_params <- df_params %>% 
    mutate(n_chain = unlist(map(seq(1,n_chains), ~rep(.x, mcmc_steps)))) %>% 
    group_by(n_chain) %>% 
    mutate(iter = 1:n()) %>% 
    ungroup()
  
  df_accept <- df_chains %>% 
    dplyr::select(accept) %>% 
    unnest(c(accept)) %>% 
    pull(accept) %>% 
    as_tibble()
  colnames(df_accept) <- c_names
  df_accept <- df_accept %>% 
    mutate(n_chain = unlist(map(seq(1,n_chains), ~rep(.x, mcmc_steps))))
  
  df_sd_prop <- df_chains %>% 
    dplyr::select(sd_proposal, n_chain) %>% 
    unnest(sd_proposal) %>% 
    mutate(names = rep(c_names,n_chains)) %>% 
    pivot_wider(names_from = names, values_from = sd_proposal)
  
  chain <- list(loglik = df_loglik, params = df_params, accept = df_accept, 
                sd_proposal = df_sd_prop, inds_to_update = lchain[[1]]$inds_to_update,
                scenario = sce, model = model, init_model = init_model, elpased_hours = elapsed)
  print("chain")
  
  chain %>%
    saveRDS(paste0(output_calibration, 'chain_scenario', i,'.rds'))
}

################################################################################
# Save fits
################################################################################
chain  <- readRDS(paste0(output_calibration, 'chain_scenario', i,'.rds'))
best_chain <- chain$loglik %>%
  filter(iter >= mcmc_adaptive_steps) %>%
  group_by(n_chain) %>%
  summarise(loglik = mean(loglik)) %>%
  filter(loglik == max(loglik)) %>%
  pull(n_chain)

df_par_thin <- chain$params %>%
  filter(iter >= mcmc_steps - mcmc_adaptive_steps, n_chain == best_chain) %>%
  dplyr::select(-n_chain)

registerDoParallel(10)

# Define the number of simulations
n_sims <- 1000

# Generate indices for sampling
inds <- sample(1:nrow(df_par_thin), n_sims, replace = FALSE)

# Initialize lists to store results
all_params <- list()
all_sims <- list()
all_obs <- list()
all_obs_notreat <- list()
# Parallel loop for simulation
results <- foreach(ind = inds) %dopar% {
  curr_params <- as.numeric(as.matrix(df_par_thin[ind,]))
  par_no_treat <- curr_params
  par_no_treat[20] <- 0
  curr_sims <- model$simulate(curr_params, interp_ts)
  sims_no_treat <- model$simulate(par_no_treat, interp_ts)

  curr_sims_df <- as_tibble(curr_sims) %>%
    mutate(across(everything(), ~as.numeric(.x))) %>%
    mutate(id = ind, date = df_dic_dates$date) %>%
    filter(date >= '2017-01-01') %>%
    dplyr::select(-c(t, starts_with(c('I[', 'R['))))

  obs <- model$observe(curr_sims, curr_params, noise = noise) %>%
    mutate(id = ind, date = week2date(week)) %>%
    filter(date >= '2017-01-01')

  obs_notreat <- model$observe(sims_no_treat, par_no_treat, noise = noise) %>%
    mutate(id = ind, date = week2date(week)) %>%
    filter(date >= '2017-01-01')

  list(curr_params = curr_params,
       curr_sims_df = curr_sims_df,
       obs = obs,
       obs_notreat = obs_notreat)
}

# Combine results
for (index in seq_along(results)) {
  all_params[[index]] <- results[[index]]$curr_params
  all_sims[[index]] <- results[[index]]$curr_sims_df
  all_obs[[index]] <- results[[index]]$obs
  all_obs_notreat[[index]] <- results[[index]]$obs_notreat
}


# Combine data frames
df_all_obs_notreat <- do.call('rbind', all_obs_notreat) %>% 
  mutate(sce = i) 
df_all_obs <- do.call('rbind', all_obs) %>% 
  mutate(sce = i) 
df_all_sims <- do.call('rbind', all_sims) %>% 
  mutate(sce = i) %>% 
  filter(id %in% inds[1:200])
df_all_sims <- df_all_sims %>% 
  mutate(week = substr(date2week(date), 1, 8)) %>%
  mutate(season = case_when(week >= '2017-W34' & week < '2018-W34' ~ '2017-2018',
                            week >= '2018-W34' & week < '2019-W34' ~ '2018-2019',
                            week >= '2019-W34' & week < '2020-W34' ~ '2019-2020',
                            week >= '2020-W34' & week < '2021-W34' ~ '2020-2021',
                            week >= '2021-W34' & week < '2022-W34' ~ '2021-2022',
                            week >= '2022-W34' & week < '2023-W34' ~ '2022-2023',
                            week >= '2023-W34' & week < '2024-W34' ~ '2023-2024'))

df_all_obs %>% 
  saveRDS(paste0(output_calibration, 'hosp_', i,'.rds'))

df_all_sims %>% 
  saveRDS(paste0(output_calibration, 'sim_', i,'.rds'))

df_all_obs_notreat %>% 
  saveRDS(paste0(output_calibration, 'hosp_notreat_', i,'.rds'))

df_averted <- df_all_obs_notreat %>% 
  rename(hosp_no_treat = obs_process) %>% 
  left_join(df_all_obs, by = c('week', 'age', 'id', 'sce', 'date')) %>% 
  filter(date >= '2023-09-11' & date <= last(data$date)) %>% 
  mutate(averted = hosp_no_treat - obs_process)

df_averted %>% 
  saveRDS(paste0(output_calibration, 'averted_', i,'.rds'))

# Compute serology
df_pop_age <- df_all_sims %>% 
  dplyr::select(date, id, sce, starts_with(c('pop_age['))) %>% 
  filter(date >= '2020-02-01' & date <= '2020-08-31') %>% 
  pivot_longer(-c(date,  sce, id), names_to = 'age', values_to = 'pop') %>% 
  mutate(age = substr(age, 9, 9))

df_S1_age <- df_all_sims %>% 
  dplyr::select(date, id, sce, matches(c('S\\[[1-7],1,'))) %>% 
  filter(date >= '2020-02-01' & date <= '2020-08-31') %>% 
  pivot_longer(-c(date, sce, id), names_sep = ',', names_to = c('age', 'exp', 'treat'), values_to = 'S1') %>% 
  mutate(age = substr(age, 3, 3)) %>% 
  group_by(date, sce, id, age, exp) %>% 
  summarise(S1 = sum(S1)) %>% 
  filter(exp == 1)

df_psero_sim <- df_S1_age %>% 
  left_join(df_pop_age, by = c('date', 'age', "id", "sce")) %>% 
  mutate(p_sero = (pop - S1) / pop) %>% 
  mutate(age4ag = case_when(age %in% c(1,2,3,4) ~ '[0,1)',
                            age %in% c(5) ~ '[1,2)',
                            age %in% c(6) ~ '[2,3)',
                            age == 7 ~ '[3,Inf]')) %>% 
  group_by(sce, age4ag, id) %>% 
  summarise(p_sero = mean(p_sero))

df_psero_sim %>% 
  saveRDS(paste0(output_calibration, 'psero_sim_', i,'.rds'))


print('Job ended well.')

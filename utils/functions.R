col_param_name <- function(listParams) {
  size_list <- length(listParams)
  l_name <- list()
  for(i in 1:size_list){
    l_name[[i]] <- paste(listParams[[i]], collapse = '_')
  }
  return(unlist(l_name))
}

#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector f_scale_season_cos_cpp(NumericVector t, double t_peak) {
  int n = t.size();
  NumericVector result(n);
  
  for (int i = 0; i < n; ++i) {
    // double numerator = (exp((cos(2 * M_PI * (t[i] - t_peak) / 365.25) - 1) / sigma2) - exp((cos(M_PI) - 1) / sigma2));
    // double denominator = (1 - exp((cos(M_PI) - 1) / sigma2));
    // result[i] = numerator / denominator;
  result[i] = 0.5 * (cos(2 * M_PI * (t[i] - t_peak) / 365.25) + 1);
  }
  
  return result;
}

// [[Rcpp::export]]
NumericVector compute_ihr_4q_old_cpp(double eps0, double eps, double epsOld1, double epsOld2, double epsOld3) {
  int n = 7;
  NumericVector ihr(n);
  
  for (int i = 0; i < n; i++) {
    if(i < 4){
      ihr[i] = exp(-(eps0 + eps * (i + 1)));
    } else if (i == 4) {
      ihr[i] = exp(-epsOld1);
    } else if (i == 5){
      ihr[i] = exp(-epsOld2);
    } else if(i == 6){
      ihr[i] = exp(-epsOld3);
    }
  }
  return ihr;
}




